FROM ubuntu:20.04

WORKDIR /app

RUN apt -y update
RUN apt -y upgrade
RUN apt install -y ffmpeg

RUN apt -y install curl
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt install -y nodejs
RUN node --version

COPY package*.json /app
COPY . /app

RUN npm install

ENTRYPOINT ["npm"]

CMD [ "run", "dev" ]