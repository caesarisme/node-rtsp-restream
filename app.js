const Express = require("express");
const Stream = require("node-rtsp-stream");
const path = require("path");
const multer = require("multer");
const FormData = require("form-data");
const fs = require("fs");
const fetch = require("node-fetch");

const start = async () => {
  // App
  const app = new Express();
  app.use(Express.json());
  app.use(Express.urlencoded({ extended: true }));
  app.use(Express.static(path.join(__dirname, "/rtsp")));
  // app.use(Express.static(path.join(__dirname, "/rec")));

  // Uploads
  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, "./uploads");
    },
    filename: function (req, file, cb) {
      const ext = file.mimetype.split("/")[1];
      cb(null, `${file.fieldname}-${Date.now()}.${ext}`);
    },
  });
  const upload = multer({ storage });

  try {
    //   // Stream
    const stream = new Stream({
      name: "name",
      // streamUrl:
      //   "rtsp://170.93.143.139/rtplive/470011e600ef003a004ee33696235daa",
      streamUrl: "rtsp://192.168.1.159:5554",
      // streamUrl:
      //   "rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov",
      wsPort: 9999,
      ffmpegOptions: {
        // options ffmpeg flags
        // "-f": "mpegts",
        // "-b:a": "0",
        // "-ss": "",
        "-stats": "", // an option with no neccessary value uses a blank string
        "-r": 24, // options with required values specify the value after the key
      },
    });

    app.post("/recognize", upload.single("image"), async (req, res) => {
      const { path: imagePath } = req.file;
      let body = new FormData();
      body.append("upload", fs.createReadStream("./" + imagePath));

      // Or body.append('upload', base64Image);
      const result = await fetch("http://192.168.1.166:8080/alpr", {
        method: "POST",
        body: body,
      })
        .then((resp) => resp.json())
        .then((json) => {
          return json;
        })
        .catch((err) => {
          console.log(err);
        });

      res.status(200).json(result);
    });

    app.get("/stop", (req, res) => {
      stream.stop();
    });

    const PORT = 3000;
    app.listen(PORT, () =>
      console.log(`🚀 Server ready on http://localhost:${PORT}`)
    );
  } catch (e) {
    console.log("Error");
  }
};

start();
